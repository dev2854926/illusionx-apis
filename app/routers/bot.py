from fastapi import FastAPI, Form, Response, requests, status, HTTPException, Depends, APIRouter
from psycopg2 import IntegrityError
from sqlalchemy.orm import Session
from .. import models, schemas
from ..database import SessionLocal, get_db
from app import database, oauth2, utils
from sqlalchemy.orm import session
from fastapi.security.oauth2 import OAuth2PasswordRequestForm


router = APIRouter(
    prefix="/bot",
    tags=['bot']
)




@router.post("/create-new-bot", status_code=status.HTTP_201_CREATED)
async def create_new_bot(createRequest: schemas.Bot, db: Session = Depends(get_db), current_user: int = Depends(oauth2.get_current_user)):
    if current_user is None:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="Not authenticated")

    query_user = db.query(models.User).filter(
        models.User.user_id == current_user.user_id).first()

    # Make sure the user exists
    if query_user is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"User with id {current_user.user_id} does not exist")
    

    # Create a new bot 
    db_bot = models.Bot(**createRequest.dict())
    db.add(db_bot)
    db.commit()
    db.refresh(db_bot)

    return db_bot


@router.delete("/delete-bot/{name}", status_code=status.HTTP_201_CREATED)
async def create_new_bot(name: str, db: Session = Depends(get_db), current_user: int = Depends(oauth2.get_current_user)):
    if current_user is None:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="Not authenticated")

    query_user = db.query(models.User).filter(
        models.User.user_id == current_user.user_id).first()

    # Make sure the user exists
    if query_user is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"User with id {current_user.user_id} does not exist")
    

    # Delete the bot by name
    db.query(models.Bot).filter(models.Bot.name == name).delete()
    db.commit()

    return {"message": "Bot deleted successfully"}


   

@router.get("/get-all-bots")
async def get_all_bots(db: Session = Depends(get_db), current_user: int = Depends(oauth2.get_current_user)):
    if current_user is None:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="Not authenticated")

    # Retrieve all bots belonging to the current user
    bots = db.query(models.Bot).filter(models.Bot.user_id == current_user.user_id).all()

    return bots



@router.post("/chat")
async def chat_with_gpt(request: schemas.ChatRequest):
    api_key = "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"
    model = "gpt-3.5-turbo"
    max_tokens = 100

    url = "https://api.openai.com/v1/engines/{}/completions".format(model)
    headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer {}".format(api_key),
    }

    data = {
        "prompt": request.prompt,
        "max_tokens": max_tokens,
    }

    response = requests.post(url, headers=headers, json=data)

    if response.status_code == 200:
        return {"response": response.json()["choices"][0]["text"]}
    else:
        raise HTTPException(status_code=response.status_code, detail="Error: {}".format(response.text))