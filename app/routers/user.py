from fastapi import FastAPI, Form, Response, status, HTTPException, Depends, APIRouter
from psycopg2 import IntegrityError
from sqlalchemy.orm import Session
from .. import models, schemas
from ..database import SessionLocal, get_db
from app import database, oauth2, utils
from sqlalchemy.orm import session
from fastapi.security.oauth2 import OAuth2PasswordRequestForm


router = APIRouter(
    prefix="/users",
    tags=['users']
)




@router.post("/signup", status_code=status.HTTP_201_CREATED, response_model=schemas.SignupResponse)
async def signup(signup_data: schemas.SignupRequest, db: Session = Depends(get_db)):
    try:
        # Hash the password
        hashed_password = utils.hash(signup_data.user_profile.password)
        signup_data.user_profile.password = hashed_password

        # Create a new User record
        db_user = models.User(**signup_data.user_profile.dict())
        db.add(db_user)
        db.commit()
        db.refresh(db_user)

        # Create a new UserData record associated with the User
        db_user_data = models.User_Data(
            user_id=db_user.user_id, **signup_data.user_data.dict())
        db.add(db_user_data)
        db.commit()
        db.refresh(db_user_data)

        # Combine db_user and db_user_data into a response dictionary
        response_dict = {
            "user_profile_response": db_user,
            "user_data_response": db_user_data
        }

    except IntegrityError as e:
        db.rollback()
        raise HTTPException(
            status_code=400, detail="Username or email already exists")

    return response_dict


@router.post('/login', response_model=schemas.Token)
def login(user_credentials: OAuth2PasswordRequestForm = Depends(),
          db: session = Depends(database.get_db)):

    user = db.query(models.User).filter(
        models.User.email == user_credentials.username).first()

    if not user:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail=f"Invalid Credentials"
        )
    if not utils.verify(user_credentials.password, user.password):
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail=f"Invalid Credentials"
        )
    # create token
    access_token = oauth2.create_access_token(data={"user_id": user.user_id, "email": user.email})
    # return token
    return {"access_token": access_token, "token_type": "bearer"}


@router.delete('/delete/{id}', status_code=status.HTTP_204_NO_CONTENT)
def delete_account(id: int, db: Session = Depends(get_db), current_user: int = Depends(oauth2.get_current_user)):

    if current_user is None:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="Not authenticated")

    query_user = db.query(models.User).filter(
        models.User.user_id == id).first()

    # Make sure the user exists
    if query_user is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"User with id {id} does not exist")

    query_userData = db.query(models.User_Data).filter(
        models.User_Data.user_id == id).first()

    db.delete(query_user)
    db.commit()
    db.delete(query_userData)
    db.commit()

    return Response(status_code=status.HTTP_204_NO_CONTENT)



@router.put('/update-user-data/{id}', response_model=schemas.SignupResponse)
def edit_user_account(id: int, updated_user: schemas.SignupRequest, db: Session = Depends(get_db)):
    # Query the user and user data
    query_user = db.query(models.User).filter(models.User.user_id == id).first()
    query_user_data = db.query(models.User_Data).filter(models.User_Data.user_id == id).first()

    # Make sure the user exists
    if query_user is None:
        raise HTTPException(status_code=404, detail=f"User with id {id} does not exist")

    # Update user attributes
    if updated_user.user_profile:
        for key, value in updated_user.user_profile.dict().items():
            setattr(query_user, key, value)

    # Update user data attributes
    if updated_user.user_data:
        for key, value in updated_user.user_data.dict().items():
            setattr(query_user_data, key, value)

    db.commit()
    db.refresh(query_user)
    db.refresh(query_user_data)

    response = {
        "user_profile_response": query_user,
        "user_data_response": query_user_data
    }

    return response



# @router.post("/change-password")
# def reset_password(changePasswordRequest: schemas.ChangePassword, 
#                    db: Session = Depends(get_db), 
#                    current_user: int = Depends(oauth2.get_current_user)):
    
#     user_query = db.query(models.User).filter(models.User.user_id == current_user).first()

#     if current_user is None:
#         raise HTTPException(
#             status_code=status.HTTP_401_UNAUTHORIZED, detail="Not authenticated")
    
#     # Hash the old and new passwords using your custom hash function
#     old_password_hash = utils.hash(changePasswordRequest.old_password)
#     new_password_hash = utils.hash(changePasswordRequest.new_password)

#     # Verify the old password
#     if old_password_hash != user_query.password:
#        raise HTTPException(
#            status_code=400,
#            detail="Old password is incorrect",
#        )
    
#     # Update the password with the new hashed password
#     user_query.password = new_password_hash
#     db.commit()
#     db.refresh(user_query)

#     return {"message": "Password updated successfully"}




# from fastapi import Form

# @router.post("/change-password")
# def reset_password(
#     old_password: str = Form(...),
#     new_password: str = Form(...),
#     db: Session = Depends(get_db),
#     current_user: int = Depends(oauth2.get_current_user)
# ):
#     if current_user is None:
#         raise HTTPException(
#             status_code=status.HTTP_401_UNAUTHORIZED,
#             detail="Not authenticated"
#         )

#     # Fetch the user from the database based on the user_id (current_user)
#     user_query = db.query(models.User).filter(models.User.user_id == current_user).first()

#     if not user_query:
#         raise HTTPException(
#             status_code=status.HTTP_404_NOT_FOUND,
#             detail="User not found"
#         )

#     # Hash the old and new passwords using your custom hash function
#     old_password_hash = utils.hash(old_password)
#     new_password_hash = utils.hash(new_password)

#     # Verify the old password
#     if old_password_hash != user_query.password:
#         raise HTTPException(
#             status_code=400,
#             detail="Old password is incorrect",
#         )

#     # Update the password with the new hashed password
#     user_query.password = new_password_hash

#     try:
#         db.commit()
#         db.refresh(user_query)
#     except Exception as e:
#         db.rollback()
#         raise HTTPException(
#             status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
#             detail="Error updating the password"
#         )

#     return {"message": "Password updated successfully"}
