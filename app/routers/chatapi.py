from fastapi import Request, requests
from fastapi import FastAPI, Form, Response, requests, status, HTTPException, Depends, APIRouter
from psycopg2 import IntegrityError
from sqlalchemy.orm import Session
from .. import models, schemas
from ..database import SessionLocal, get_db
from app import database, oauth2, utils
from sqlalchemy.orm import session
from fastapi.security.oauth2 import OAuth2PasswordRequestForm
from fastapi import FastAPI, Request
import palm
import google.generativeai as palm



router = APIRouter(
    prefix="/chatapi",
    tags=['chatapi']
)

@router.post("/chat")
def chat(request_data: dict):
    # Unpack the dictionary
    prompt = request_data.get("prompt")
    api = request_data.get("api")

    # Assuming `palm` is an instance of your Palm library
    palm.configure(api_key=api)
    
    models = [m for m in palm.list_models() if 'generateText' in m.supported_generation_methods]
    
    if not models:
        raise HTTPException(status_code=500, detail="No suitable model found.")
    
    model = models[0].name
    completion = palm.generate_text(model=model, prompt=prompt, max_output_tokens=800, temperature=0)
    
    return {"result": completion.result}