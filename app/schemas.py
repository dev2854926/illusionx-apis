from pydantic import BaseModel, EmailStr
from datetime import date, datetime


# Input for User Table
class UserCreate(BaseModel):
    #username: str
    password: str
    email: EmailStr


# Input for User Data Table
class UserDataCreate(BaseModel):
    first_name: str
    last_name: str
    gender: str 
    birth_date: date 
    phone_number: str 
    nationality: str
    occupation: str
    profile_pic: str 

# The Signup request
class SignupRequest(BaseModel):
    user_profile: UserCreate
    user_data: UserDataCreate

# User table response
class UserResponse(BaseModel):
    user_id: int
    #username: str
    #password: str
    email: EmailStr
    created_at: datetime



# User Data table response
class UserDataResponse(BaseModel):
    id: int
    first_name: str
    last_name: str
    gender: str 
    birth_date: date
    phone_number: str 
    last_login: datetime
    nationality: str
    occupation: str
    profile_pic: str 


# Signup response
class SignupResponse(BaseModel):
    user_profile_response: UserResponse
    user_data_response: UserDataResponse


# Input for Login
class UserLogin(BaseModel):
    email: EmailStr
    password: str

# Response for Login
class UserLoginResponse(BaseModel):
    user_id: int
    email: EmailStr
    created_at: datetime

    class Config:
        orm_mode = True


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    user_id: int 
    

class ChangePassword(BaseModel):
    old_password: str
    new_password: str

class Bot(BaseModel):
    user_id: int
    name: str
    specialization: str
    gender: str
    language: str

class BotRes(BaseModel):
    user_id: int
    name: str
    specialization: str
    gender: str
    language: str

class ChatRequest(BaseModel):
    prompt: str