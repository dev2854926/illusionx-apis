from sqlalchemy import Column, Integer, String, Boolean, ForeignKey, Date, DateTime
from .database import Base
from sqlalchemy.orm import relationship
from sqlalchemy.sql.expression import text
from sqlalchemy.sql.sqltypes import TIMESTAMP
from datetime import datetime


class User(Base):
    __tablename__ = "users"

    user_id = Column(Integer, primary_key=True, nullable=False)
    #username = Column(String, unique=True)
    email = Column(String, unique=True)
    password = Column(String, nullable=False)
    created_at = Column(DateTime(timezone=True), nullable=False, server_default=text('now()'))
    
    # Define a relationship with User_Data
    user_data = relationship("User_Data", back_populates="user")

class User_Data(Base):
    __tablename__ = "users_data"

    id = Column(Integer, primary_key=True, index=True)
    first_name = Column(String, nullable=False)
    last_name = Column(String, nullable=False)
    gender = Column(String, nullable=False)
    birth_date = Column(Date, nullable=False)
    phone_number = Column(String, nullable=False)
    last_login = Column(DateTime, default=datetime.utcnow)
    nationality = Column(String, nullable=False)
    occupation = Column(String, nullable=False)
    profile_pic = Column(String, nullable=True)
    
    # Define a foreign key relationship with User
    user_id = Column(Integer, ForeignKey("users.user_id", ondelete="CASCADE"))
    
    # Establish the relationship with the User class
    user = relationship("User", back_populates="user_data")






class Bot(Base):
    __tablename__= "bot"

    bot_id = Column(Integer, primary_key=True, nullable=False, index=True, autoincrement=True)
    name = Column(String, nullable=False)
    specialization = Column(String, nullable=False)
    gender = Column(String, nullable=False)
    language = Column(String, nullable=False)
    last_use = Column(DateTime, default=datetime.utcnow, nullable=False)
    created_at = Column(TIMESTAMP(timezone=True), nullable=False, server_default=text('now()'))
    user_id = Column(Integer, ForeignKey("users.user_id", ondelete="CASCADE"), nullable=False)

    owner = relationship("User")



